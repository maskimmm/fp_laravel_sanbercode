@extends('layouts.master')

@section('judul')

Profile Menu
@endsection

@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Customize your Persona!</h3>

    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
        <i class="fas fa-minus"></i>
      </button>
      <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
        <i class="fas fa-times"></i>
      </button>
    </div>
  </div>
  
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
    @endif  
    <form action="/profile" method="POST">
      @csrf
      <div class="form-group">
        <label >Name</label>
        <input type="text" name="nama" value="{{$profiles->nama}}" class="form-control">
      </div>
      @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="form-group">
        <label >Description</label>
        <textarea name="biodata" cols="20" rows="10" class="form-control">{{$profiles->biodata}}</textarea>
      </div>
      @error('biodata')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="form-group">
        <label >Age</label>
        <input type="text" name="umur" value="{{$profiles->umur}}" class="form-control">
      </div>
      @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="form-group">
        <label >Adress</label>
        <textarea name="alamat" cols="10" rows="5" class="form-control">{{$profiles->alamat}}</textarea>
      </div>
      @error('alamat')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <button type="submit" class="btn btn-primary">Save</button>
    </form>
  </div>

  <!-- /.card-body -->
  <!-- <div class="card-footer">
    Footer
  </div> -->
  <!-- /.card-footer-->
</div>

@push('script')
<script src="https://cdn.tiny.cloud/1/hvcc12zud5l3ncm15paogcw3ax7a7e6jdype58n1ik1z9b7s/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>\
<script>
  tinymce.init({
    selector: 'textarea',
    plugins: 'a11ychecker advcode casechange export formatpainter image editimage linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tableofcontents tinycomments tinymcespellchecker',
    toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter image editimage pageembed permanentpen table tableofcontents',
    toolbar_mode: 'floating',
    tinycomments_mode: 'embedded',
    tinycomments_author: 'Author name',
  });
</script>
@endpush
  

@endsection