@extends('layouts.master')

@section('judul')
  Answers
@endsection

@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">{{$posts->pertanyaan}} </h3>

    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
        <i class="fas fa-minus"></i>
      </button>
      <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
        <i class="fas fa-times"></i>
      </button>
    </div>
  </div>
  <div class="card-body">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif  
    <form action="/answer/{{$posts->idpost}}" method="POST" enctype="multipart/form-data">
      @csrf
      <div class="form-group">
        <label >Answer</label>
        <input type="text" name="jawaban" value="{{old('jawaban')}}" class="form-control">
      </div>
      @error('jawaban')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Attachment</label>
      <input type="file" name="gambar" class="form-control">
    </div>
    @error('kategori')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>
  <!-- /.card-body -->
  <!-- <div class="card-footer">
    Footer
  </div> -->
  <!-- /.card-footer-->
</div>
@endsection