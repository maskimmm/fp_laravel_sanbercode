@extends('layouts.master')

@section('judul')
  Answers
@endsection

@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">List of Your Helps Solving Others Problems</h3>

    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
        <i class="fas fa-minus"></i>
      </button>
      <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
        <i class="fas fa-times"></i>
      </button>
    </div>
  </div>
  
  <div class="card-body">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th scope="col">No</th>
          <th scope="col">Question</th>
          <th scope="col">Answer</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <tbody>
        @forelse ($komentar as $key =>$item)
        <tr>
          <td>{{$item->idkomentar}}</td>
          <td>{{Str::limit($item->pertanyaan, 30)}}</td>
          <td>{{Str::limit($item->jawaban, 30)}}</td>
          <td> 
            <form action="/answer/{{$item->idkomentar}}" method="POST">
              @csrf
              <a href="/answer/{{$item->idkomentar}}" class="btn btn-info btn-sm">Detail</a>
              <a href="/answer/{{$item->idkomentar}}/edit" class="btn btn-warning btn-sm">Edit</a>
              @method('delete')
              <input type="submit" value="Delete" class="btn btn-danger btn-sm">
            </form>
          </td>  
        </tr>
        @empty
            <h3>You never answer any question! Go help others!</h3>
        @endempty
      </tbody>
    </table>
  </div>
  <!-- /.card-body -->
  <!-- <div class="card-footer">
    Footer
  </div> -->
  <!-- /.card-footer-->
</div>
@endsection