@extends('layouts.master')

@section('judul')
    Answers
@endsection

@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">#{{$komentar->idkomentar}}</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>

    <div class="card-body">
      <h1 class="text-dark">{{$komentar->judul}}</h1>
      <h6>Answer By: {{$id = auth()->user()->name}} - 
          Created At: {{$komentar->created_at}} 
      </h6>
      <h6>{{$komentar->jawaban}}</h6>
      
      @if(empty($komentar->gambar))
      @else
          <img src="{{asset('/image/'. $komentar->gambar)}}" class="img card" alt="..." width="250" height="300"> 
      @endif
      
      <a href="/answer" class="btn btn-danger btn-sm mb-3 ">Back</a>
    </div>
    <!-- /.card-body -->
    <!-- <div class="card-footer">
      Footer
    </div> -->
    <!-- /.card-footer-->
</div>

@endsection