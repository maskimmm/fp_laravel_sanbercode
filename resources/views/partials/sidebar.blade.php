<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="../../index3.html" class="brand-link">
    <span class="brand-text font-weight-light">Forum.in</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item">
          <a href="/profile" class="nav-link">
            <i class="nav-icon fas fa-user-alt"></i>
            <p>
              Profile
            </p>
          </a>
        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Discover
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/qdiscovery" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Discover Questions</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/category" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Category</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-question"></i>
            <p>
              Your Questions
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/question/create" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Ask Question</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/question" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Your Question</p>
              </a>
            </li>
          </ul>
          <li class="nav-item">
            <a href="/answer" class="nav-link">
              <i class="nav-icon fas fa-lightbulb"></i>
              <p>
                Answer
              </p>
            </a>
          </li>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-chart-pie"></i>
            <p>
              Contributor
            </p>
          </a>
        </li>
        <li class="nav-item">
          @auth
            <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
              <i class="far fa-circle nav-icon"></i>  
              {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
            </form> 
          @endauth
        </li>
      </ul>
    </nav>
      <!-- /.sidebar-menu -->
  </div>
    <!-- /.sidebar -->
  </aside>