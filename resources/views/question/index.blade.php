@extends('layouts.master')

@section('judul')
  Questions
@endsection


@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">List of Your Questions</h3>
    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
        <i class="fas fa-minus"></i>
      </button>
      <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
        <i class="fas fa-times"></i>
      </button>
    </div>
  </div>

  <div class="card-body">
    <a href="/question/create" class="btn btn-primary btn-sm mb-3">Ask A Question</a>
    <table class="table table-bordered">
      <thead>
        <tr>
          <th scope="col">No</th>
          <th scope="col">Category</th>
          <th scope="col">Title</th>
          <th scope="col">Question</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <tbody>
        @forelse ($posts as $key =>$item)
          <tr>
            <td>{{$item->idpost}}</td>
            <td>{{$item->kategori->kategori}}</td>
            <td>{{$item->judul}}</td>
            <td>{{Str::limit($item->pertanyaan, 30)}}</td>
            <td> 
              <form action="/question/{{$item->idpost}}" method="POST">
                @csrf
                <a href="/question/{{$item->idpost}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/question/{{$item->idpost}}/edit" class="btn btn-warning btn-sm">Edit</a>
                @method('delete')
                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
              </form>
            </td>  
          </tr>
        @empty
          <h3>Shy Asking, Lost in Road! - Anonymous</h3>
        @endempty
      </tbody>
    </table>
  </div>
  <!-- /.card-body -->
  <!-- <div class="card-footer">
    Footer
  </div> -->
  <!-- /.card-footer-->
</div>
@endsection