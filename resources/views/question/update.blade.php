@extends('layouts.master')

@section('judul')
  Questions
@endsection

@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Update Question #{{$idpost}}</h3>
    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
        <i class="fas fa-minus"></i>
      </button>
      <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
        <i class="fas fa-times"></i>
      </button>
    </div>
  </div>

  <div class="card-body">
    {{-- @yield('contentcard') --}}
    <form action="/question/{{$idpost}}" method="POST" enctype="multipart/form-data">
      @csrf
      @method('PUT')
      <div class="form-group">
        <label >Title</label>
        <input type="text" name="judul" value="{{$posts->judul}}" class="form-control">
      </div>
      @error('judul')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="form-group">
        <label >Question</label>
        <textarea name="pertanyaan" cols="30" rows="10" class="form-control">{{$posts->pertanyaan}}</textarea>
      </div>
      @error('pertanyaan')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="form-group">
        <label >Attachment</label>
        <input type="file" name="gambar" class="form-control">
      </div>

      <div class="form-group">
        <label for="kategori">Category</label>
        <select class="form-control" name="kategori">
            <option value ='{{$kategori[$posts->kategori_idkategori-1]->idkategori}}'>{{$kategori[$posts->kategori_idkategori-1]->kategori}}</option> 
        </select>
      </div>
      @error('kategori')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <button type="submit" class="btn btn-primary">Update</button>
    </form>
  </div>
  <!-- /.card-body -->
  <!-- <div class="card-footer">
    Footer
  </div> -->
  <!-- /.card-footer-->
</div>
@endsection