@extends('layouts.master')

@section('judul')

Questions
@endsection

@section('subjudul')

Please kindly ask any question that you need!
@endsection

@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Ask a Question</h3>
    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
        <i class="fas fa-minus"></i>
      </button>
      <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
        <i class="fas fa-times"></i>
      </button>
    </div>
  </div>

  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
    @endif  

    <form action="/question" method="POST" enctype="multipart/form-data">
      @csrf
      <div class="form-group">
        <label >Question</label>
        <input type="text" name="judul" value="{{old('judul')}}" class="form-control">
      </div>
      @error('judul')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="form-group">
        <label >Description</label>
        <textarea name="pertanyaan" cols="30" rows="10" class="form-control">{{old('pertanyaan')}}</textarea>
      </div>
      @error('pertanyaan')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="form-group">
        <label >Attachment</label>
        <input type="file" name="gambar" class="form-control">
      </div>

      <div class="form-group">
        <label for="kategori">Category:</label>
        <select class="form-control" name="kategori">
          @forelse ($kategories as $key=>$value)
            <p>{{$value->idkategori}}</p>
            <option value ='{{$value->idkategori}}'>{{$value->kategori}}</option> 
          @empty
          @endforelse
        </select>
      </div>
      @error('kategori')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>
  <!-- /.card-body -->
  <!-- <div class="card-footer">
    Footer
  </div> -->
  <!-- /.card-footer-->
</div>
@endsection