@extends('layouts.master')

@section('judul')
Detail of question
@endsection

@section('subjudul')
{{-- {{$kategori[$posts->kategori_idkategori]->kategori}} --}}
@endsection

@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">#{{$posts->idpost}} - {{$posts->judul}}</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>

    <div class="card-body">
      <h1 class="text-dark">{{$posts->judul}}</h1>
      <h6>Question By: {{$id = auth()->user()->name}} - 
          Created At: {{$posts->created_at}} 
      </h6>
      <h6>{{$posts->pertanyaan}}</h6>
      
      @if(empty($posts->gambar))
      @else
          <img src="{{asset('/image/'. $posts->gambar)}}" class="img card" alt="..." width="250" height="300"> 
      @endif
      
      <a href="/question" class="btn btn-danger btn-sm mb-3 ">Back</a>
      {{-- <a href="/answer/create/{{$posts->idpost}}" class="btn btn-success btn-sm mb-3 ">Answer</a> --}}
    </div>
    <!-- /.card-body -->
    <!-- <div class="card-footer">
      Footer
    </div> -->
    <!-- /.card-footer-->
</div>

@forelse ($komentar as $key=>$item)
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">#{{$item->idkomentar}} - Answer By: {{$item->user->name}}</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>

    <div class="card-body">
      <h6>Created At: {{$item->created_at}}</h6>
      <p>{{$item->jawaban}}</p>
      
      @if(empty($item->gambar))
      @else
          <img src="{{asset('/image/'. $item->gambar)}}" class="img card" alt="..." width="250" height="300"> 
      @endif
    </div>
    <!-- /.card-body -->
    <!-- <div class="card-footer">
      Footer
    </div> -->
    <!-- /.card-footer-->
  </div>
@empty
  <div class="card">
    <div class="card-header">
    <h3 class="card-title">No Answer Yet</h3>
    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
        <i class="fas fa-minus"></i>
      </button>
      <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
        <i class="fas fa-times"></i>
      </button>
    </div>
  </div>

</div>
@endempty

@endsection