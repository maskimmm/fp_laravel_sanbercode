@extends('layouts.master')

@section('judul')

Category
@endsection

@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Create New Category</h3>
    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
        <i class="fas fa-minus"></i>
      </button>
      <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
        <i class="fas fa-times"></i>
      </button>
    </div>
  </div>

  <div class="card-body">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif  
    <form action="/category" method="POST" >
      @csrf
      <div class="form-group">
        <label >Category</label>
        <input type="text" name="kategori" value="{{old('kategori')}}" class="form-control">
      </div>
      @error('kategori')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>
  <!-- /.card-body -->
  <!-- <div class="card-footer">
    Footer
  </div> -->
  <!-- /.card-footer-->
</div>
@endsection