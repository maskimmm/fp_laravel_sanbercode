@extends('layouts.master')

@section('judul')

Categories
@endsection


@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Discover Categories</h3>
    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
        <i class="fas fa-minus"></i>
      </button>
      <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
        <i class="fas fa-times"></i>
      </button>
    </div>
  </div>
  
  <div class="card-body">
    <a href="/category/create" class="btn btn-primary btn-sm mb-3">Create New Category</a>
    <table class="table table-bordered">
      <thead>
        <tr>
          <th scope="col">No</th>
          <th scope="col">Category</th>
          {{-- <th scope="col">Action</th> --}}
        </tr>
      </thead>
      <tbody>
        @forelse ($kategori as $key =>$item)
        <tr>
          <td>{{$item->idkategori}}</td>
          <td>{{$item->kategori}}</td>
          {{-- <td> 
            <form action="/category/{{$item->idkategori}}" method="POST">
              @csrf
              <a href="/category/{{$item->idkategori}}/edit" class="btn btn-warning btn-sm">Edit</a>
              @method('delete')
              <input type="submit" value="Delete" class="btn btn-danger btn-sm">
            </form>
          </td>   --}}
        </tr>
        @empty
            <h3>Input your category!</h3>
        @endempty
      </tbody>
    </table>
  </div>
  <!-- /.card-body -->
  <!-- <div class="card-footer">
    Footer
  </div> -->
  <!-- /.card-footer-->
</div>
@endsection