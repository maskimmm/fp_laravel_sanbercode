@extends('layouts.master')

@section('judul')

Categories
@endsection

@section('subjudul')

New category? Just insert it!
@endsection

@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Update Category #{{$kategori->idkategori}}</h3>
    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
        <i class="fas fa-minus"></i>
      </button>
      <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
        <i class="fas fa-times"></i>
      </button>
    </div>
  </div>
  
  <div class="card-body">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif  
    <form action="/category/{{$kategori->idkategori}}" method="POST" >
      @csrf
      @method('PUT')
      <div class="form-group">
        <label >Category</label>
        <input type="text" name="kategori" value="{{$kategori->kategori}}" class="form-control">
      </div>
      @error('kategori')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <button type="submit" class="btn btn-primary">Update</button>
    </form>
  </div>
  <!-- /.card-body -->
  <!-- <div class="card-footer">
    Footer
  </div> -->
  <!-- /.card-footer-->
</div>
@endsection