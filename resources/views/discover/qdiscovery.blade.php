@extends('layouts.master')

@section('judul')
  Discover Questions
@endsection


@section('content')
@forelse ($posts as $key =>$item)
    <div class="card">
    <div class="card-header">
        <h4 class="card-title">{{$item->kategori->kategori}} - {{$item->judul}}</h4>
        <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
            <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
            <i class="fas fa-times"></i>
        </button>
        </div>
    </div>

    <div class="card-body">
        <h6>Question By: {{$item->user->name}} - 
            Created At: {{$item->created_at}} 
        </h6>
        <h6>{{$item->pertanyaan}}</h6>

        @if(empty($item->gambar))
        @else
            <img src="{{asset('/image/'. $item->gambar)}}" class="img card" alt="..." width="250" height="300"> 
        @endif

        <a href="/answer/create/{{$item->idpost}}" class="btn btn-success btn-sm mb-3 ">Answer</a>
    </div>
    <!-- /.card-body -->
    <!-- <div class="card-footer">
        Footer
    </div> -->
    <!-- /.card-footer-->
    </div>
@empty
@endempty
@endsection