<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Posts;
class Kategori extends Model
{
    protected $table = "kategori";
    protected $primaryKey = "idkategori";
    protected $fillable = ['idkategori', 'kategori'];

    public $timestamps = false;
    
    public function posts(){
        return $this->hasMany(Posts::class, 'kategori_idkategori', $primaryKey);
    }
}
