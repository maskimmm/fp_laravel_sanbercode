<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Posts;
use App\Kategori;
use App\User;

class QDiscoveryController extends Controller
{
    
    public function _construct(){
        $this->middleware('auth');
    }

    public function index(){
        $uid = auth()->user()->id;
        $posts = Posts::with('user', 'kategori')->get();
        $kategori = Kategori::all();
        // dd($posts);
        
        return view('discover.qdiscovery', ['posts' => $posts,'kategori' => $kategori, 'uid' => $uid]);
    }
}
