<?php

namespace App\Http\Controllers;
use RealRashid\SweetAlert\Facades\Alert;


use Illuminate\Http\Request;
use DB;
use App\Komentar;
use App\Kategori;
use App\Posts;

class AnswerController extends Controller
{

    public function _construct(){
        $this->middleware('auth');
    }

     public function create($id){
        $kategories = Kategori::all();
        $posts = Posts::where('idpost', $id)->first();
        return view('answer.create', ['kategories' => $kategories, 'posts' => $posts]);
    }

    public function store(Request $request, $posts){
        $uid = auth()->user()->id;
        // dd($posts);

        $this->validate($request, [
            'jawaban' => 'required',
            // 'gambar' => ['mimes:jpg,png,jpeg', 'max:2048'],
        ]);

        if(!is_null($request['gambar'])){
            $namagambar = time().'.'.$request->gambar->extension();
            $request->gambar->move(public_path('image'), $namagambar);
        }else{
            $namagambar = null;
        }

        Komentar::create([
            'users_iduser' => $uid,
            'posts_idpost' => $posts,
            'jawaban' => $request['jawaban'],
            'gambar' => $namagambar,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        Alert::success('Success', 'You success to answer the question');

        
        return redirect('/answer');
    }

    public function index(){
        $uid = auth()->user()->id;
        $komentar = Komentar::where('users_iduser',$uid)->get();
        $kategori = Kategori::all();
        $posts = Posts::all();
        
        return view('answer.index', ['komentar' => $komentar,'kategori' => $kategori, 'posts' => $posts]);
    }
    
    public function show($idkomentar){
        $uid = auth()->user()->id;
        $komentar = Komentar::where('idkomentar',$idkomentar)->first();
        $kategori = Kategori::all();

        return view('answer.detail', ['komentar' => $komentar,'kategori' => $kategori]);
    }

    public function edit($id){
        $komentar = Komentar::where('idkomentar', $id)->first();
        $kategori = Kategori::all();

        return view('answer.update', ['komentar' => $komentar,'kategori' => $kategori, 'idkomentar'=>$id]);
    }
    
    public function update(Request $request, $id){
        // dd($request->all());

        Komentar::where('idkomentar', $id)
            ->update([
                'jawaban' => $request['jawaban'],
                'updated_at' => now(),
            ]);  
            
        return redirect('/answer');
    }
    
    public function destroy($id){
        Komentar::where('idkomentar', $id)->delete();
        return redirect('/answer');
    }
}
