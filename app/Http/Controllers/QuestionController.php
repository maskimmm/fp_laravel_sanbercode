<?php

namespace App\Http\Controllers;
use RealRashid\SweetAlert\Facades\Alert;


use Illuminate\Http\Request;
use App\Posts;
use App\Kategori;
use App\Komentar;

class QuestionController extends Controller
{

    public function _construct(){
        $this->middleware('auth');
    }

     public function create(){
        $kategories = Kategori::all();
        return view('question.create', ['kategories' => $kategories]);
    }

    public function store(Request $request){
        $uid = auth()->user()->id;

        $this->validate($request, [
            'judul' => 'required',
            'kategori' => 'required',
            // 'gambar' => ['mimes:jpg,png,jpeg', 'max:2048'],
        ]);

        if(!is_null($request['gambar'])){
            $namagambar = time().'.'.$request->gambar->extension();
            $request->gambar->move(public_path('image'), $namagambar);
        }else{
            $namagambar = null;
        }

        Posts::create([
            'users_iduser' => $uid,
            'judul' => $request['judul'],
            'pertanyaan' => $request['pertanyaan'],
            'gambar' => $namagambar,
            'kategori_idkategori' => $request['kategori'],
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        Alert::success('Success', 'You success to ask a question!');

        
        return redirect('/question');
    }

    public function index(){
        $uid = auth()->user()->id;
        // $posts = Posts::where('users_iduser',$uid)->kategori();
        $posts = Posts::with('kategori')->where('users_iduser', $uid)->get();
        $kategori = Kategori::all();
        // dd($posts);
        
        return view('question.index', ['posts' => $posts,'kategori' => $kategori]);
    }
    
    public function show($idpost){
        $uid = auth()->user()->id;
        $posts = Posts::where('idpost',$idpost)->first();
        $komentar = Komentar::with('user')->where('posts_idpost',$idpost)->get();
        $kategori = Kategori::all();

        return view('question.detail', ['posts' => $posts,'kategori' => $kategori, 'komentar' => $komentar]);
    }

    public function edit($id){
        $posts = Posts::where('idpost', $id)->first();
        $kategori = Kategori::all();

        return view('question.update', ['posts' => $posts,'kategori' => $kategori, 'idpost'=>$id]);
    }

    public function update(Request $request, $id){

        // $this->validate($request, [
        //     'judul' => 'required',
        //     'kategori_idkategori' => 'required',
        // ]);

        Posts::where('idpost', $id)
            ->update([
                // 'users_iduser' => $uid,
                'judul' => $request['judul'],
                'pertanyaan' => $request['pertanyaan'],
                'gambar' => $request['gambar'],
                'kategori_idkategori' => $request['kategori'],
                'updated_at' => now(),
            ]);  
            
        return redirect('/question');
    }

    public function destroy($id){
        Posts::where('idpost', $id)->delete();
        return redirect('/question');
    }
}