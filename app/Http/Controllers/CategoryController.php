<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Komentar;
use App\Kategori;
use App\Posts;

class CategoryController extends Controller
{
    public function _construct(){
        $this->middleware('auth');
    }

    public function create(){
        $kategories = Kategori::all();
        return view('category.create', ['kategories' => $kategories]);
    }

    public function store(Request $request){
        $uid = auth()->user()->id;

        $this->validate($request, [
            'kategori' => 'required',

        ]);

        Kategori::create([
            'kategori' => $request['kategori'],
        ]);
        
        return redirect('/category');
    }

    public function index(){
        $kategori = Kategori::all();
        
        return view('category.index', ['kategori' => $kategori]);
    }
    

    public function edit($id){
        $kategori = Kategori::where('idkategori', $id)->first();
        

        return view('category.update', ['kategori' => $kategori]);
    }
    
    public function update(Request $request, $id){
        Kategori::where('idkategori', $id)
            ->update([
                'kategori' => $request['kategori'],
            ]);  
        // dd($request->all());
        return redirect('/category');
    }
    
    public function destroy($id){
        Kategori::where('idkategori', $id)->delete();
        return redirect('/category');
    }
}
