<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Profiles;
use App\Kategori;
use App\User;

class ProfileController extends Controller
{
    public function _construct(){
        $this->middleware('auth');
    }

    public function generate(){
        $uid = auth()->user()->id;
        $profiles = Profiles::where('users_iduser', $uid)->firstOr(function (){
            $uid = auth()->user();
            Profiles::firstOrCreate(
                [
                    'users_iduser' => $uid->id,
                ],
                [
                    'users_iduser' => $uid->id,
                    'nama' => $uid->name,
                ]);

        });
    }

    public function create(){
        $this->generate();
        $uid = auth()->user();
        $profiles = Profiles::where('users_iduser', $uid->id)->firstOr(function (){
            $uid = auth()->user();
            Profiles::firstOrCreate(
                [
                    'users_iduser' => $uid->id,
                ],
                [
                    'users_iduser' => $uid->id,
                    'nama' => $uid->name,
                ]);

        });
        
        return view('profile.create', ['profiles' => $profiles, 'uid' => $uid]);
    }

    public function store(Request $request){
        $uid = auth()->user()->id;

        Profiles::where('users_iduser',$uid)->update(
            [
                'users_iduser' => $uid,
                'nama' => $request['nama'],
                'biodata' => $request['biodata'],
                'umur' => $request['umur'],
                'alamat' => $request['alamat'], 
            ]);
        
        User::where('id', $uid)->update(['name'=>$request['nama']]);

        return redirect('/profile');
    }

}
