<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Posts;
class Komentar extends Model
{
    protected $table = "komentar";
    protected $primaryKey = "idkomentar";
    protected $fillable = ['jawaban', 'gambar', 'users_iduser', 'posts_idpost'];

    public function posts(){
        return $this->belongsTo(Posts::class, 'posts_idpost', 'idpost');
    }

    public function user(){
        return $this->belongsTo(User::class, 'users_iduser', 'id');
    }
}
