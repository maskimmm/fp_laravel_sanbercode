<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class profiles extends Model
{
    protected $table = "profiles";
    protected $primaryKey = "idprofile";
    protected $fillable = ['nama', 'biodata', 'umur', 'alamat', 'users_iduser'];

    public $timestamps = false;

    public function user(){
        return $this->belongsTo(User::class, 'users_iduser', 'id');
    }
}
