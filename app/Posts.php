<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Kategori;
use App\User;
use App\Komentar;
class Posts extends Model
{
    protected $table = "posts";
    protected $primaryKey = "idpost";
    protected $fillable = ['judul', 'pertanyaan', 'gambar', 'kategori_idkategori', 'users_iduser'];
    
    public function kategori(){
        return $this->belongsTo(Kategori::class, 'kategori_idkategori', 'idkategori');
    }

    public function user(){
        return $this->belongsTo(User::class, 'users_iduser', 'id');
    }

    public function komentar(){
        return $this->hasMany(Komentar::class, 'posts_idpost', 'idpost');
    }
}
