<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddToKomentarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('komentar', function (Blueprint $table) {
            $table->unsignedBigInteger('users_iduser');
            $table->foreign('users_iduser')->references('id')->on('users');
            
            $table->unsignedBigInteger('posts_idpost');
            $table->foreign('posts_idpost')->references('idpost')->on('posts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('komentar', function (Blueprint $table) {
            $table->dropForeign(['users_iduser']);
            $table->dropColumn(['users_iduser']);
            
            $table->dropForeign(['posts_idpost']);
            $table->dropColumn(['posts_idpost']);
        });
    }
}
