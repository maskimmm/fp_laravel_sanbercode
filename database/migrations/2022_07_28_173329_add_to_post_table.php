<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddToPostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->unsignedBigInteger('kategori_idkategori');
            $table->foreign('kategori_idkategori')->references('idkategori')->on('kategori');
            
            $table->unsignedBigInteger('users_iduser');
            $table->foreign('users_iduser')->references('id')->on('users');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropForeign(['kategori_idkategori']);
            $table->dropColumn(['kategori_idkategori']);
            
            $table->dropForeign(['users_iduser']);
            $table->dropColumn(['users_iduser']);
        });
    }
}
