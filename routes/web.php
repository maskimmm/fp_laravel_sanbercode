<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/master', function () {
    return view('layouts.master');
})->middleware('auth');

// CRUD Pertanyaan

// Form pertanyaan
Route::get('/question/create', 'QuestionController@create')->middleware('auth');
// Kirim inputan ke list pertanyaan
Route::post('/question', 'QuestionController@store')->middleware('auth');
// Tampill semua list pertanyaan
Route::get('/question', 'QuestionController@index');
// Detail pertanyaan menurut id
Route::get('/question/{id}', 'QuestionController@show');
// Masuk ke form pertanyaan menurut id
Route::get('/question/{id}/edit', 'QuestionController@edit')->middleware('auth');
// Update list pertanyaan menurut id
Route::put('/question/{id}', 'QuestionController@update')->middleware('auth');
// Menghapus list menurut id
Route::delete('/question/{id}', 'QuestionController@destroy')->middleware('auth');

// CRUD Profile

// Kirim inputan ke profil
Route::post('/profile', 'ProfileController@store')->middleware('auth');
//Read profil
Route::get('/profile', 'ProfileController@create')->middleware('auth');

// CRUD Jawaban

// Form jawaban
Route::get('/answer/create/{qid}', 'AnswerController@create')->middleware('auth');
// Kirim inputan ke jawaban
Route::post('/answer/{qid}', 'AnswerController@store')->middleware('auth');
// Tampill jawaban
Route::get('/answer', 'AnswerController@index');
// Edit jawaban
Route::get('/answer/{qid}', 'AnswerController@show')->middleware('auth');

Route::get('/answer/{qid}/edit', 'AnswerController@edit')->middleware('auth');
Route::put('/answer/{id}', 'AnswerController@update')->middleware('auth');
Route::delete('/answer/{id}', 'AnswerController@destroy')->middleware('auth');

// CRUD Kategori

// Form kategori
Route::post('/category', 'CategoryController@store')->middleware('auth');
// Read kategori
Route::get('/category/create', 'CategoryController@create')->middleware('auth');
// Tampill kategori
Route::get('/category', 'CategoryController@index');
// Edit kategori
Route::get('/category/{qid}/edit', 'CategoryController@edit')->middleware('auth');
Route::put('/category/{id}', 'CategoryController@update')->middleware('auth');
Route::delete('/category/{id}', 'CategoryController@destroy')->middleware('auth');

// CRUD QDiscovery

Route::get('/qdiscovery', 'QDiscoveryController@index')->middleware('auth');
