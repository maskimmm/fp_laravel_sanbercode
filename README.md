# Final Project

# Kelompok 13

## Anggota Kelompok

- Dimas Arya Mada Saputra
- Rifqi Alhakim Hariyantoputera

# Tema Project

Forum Tanya Jawab

# ERD
![alt text](/public/image/ERD Forum.png)

# Link Link

Link Video Demo: https://youtu.be/hjEu2d93g6A  
Link Deploy: https://forumin.forumin3.sanbercodeapp.com/
